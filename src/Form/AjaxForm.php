<?php

namespace Drupal\ajax_form_validation\Form;

use Drupal\Core\Form\FormBase;

use Drupal\Core\Form\FormStateInterface;

use Drupal\Core\Ajax\AjaxResponse;

use Drupal\Core\Ajax\CssCommand;

use Drupal\Core\Ajax\HtmlCommand;

use Drupal\file\Entity\File;

use Drupal\node\Entity\Node;

/**
 * Class AjaxForm.
 *
 * @package Drupal\ajax_form_validation\Form
 */
class AjaxForm extends FormBase {

  public function getFormId() {
    return 'ajax_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['title']                  = [
      '#type'     => 'textfield',
      '#title'    => $this->t('Title'),
      '#ajax'     => array(
        'callback' => array($this, 'validateTitleAjax'),
        'event'    => 'change',
        'progress' => array(
          'type'    => 'throbber',
          'message' => $this->t('Verifying title...'),
        ),
      ),
      '#suffix'   => '<span class="title-ajax-validation"></span>',
      '#required' => TRUE,
    ];
    $form['name']                   = [
      '#type'     => 'textfield',
      '#title'    => $this->t('Name'),
      '#ajax'     => array(
        'callback' => array($this, 'validateNameAjax'),
        'event'    => 'change',
        'progress' => array(
          'type'    => 'throbber',
          'message' => $this->t('Verifying name...'),
        ),
      ),
      '#suffix'   => '<span class="name-ajax-validation"></span>',
      '#required' => TRUE,
    ];
    $form['email']                  = [
      '#type'     => 'email',
      '#title'    => $this->t('Email'),
      '#ajax'     => [
        'callback' => array($this, 'validateEmailAjax'),
        'event'    => 'change',
        'progress' => array(
          'type'    => 'throbber',
          'message' => $this->t('Verifying email...'),
        ),
      ],
      '#suffix'   => '<span class="email-ajax-validation"></span>',
      '#required' => TRUE,
    ];
    $form['phone']                  = [
      '#type'        => 'tel',
      '#title'       => $this->t('Phone'),
      '#description' => $this->t('Please enter number within format (999) 999-9999.'),
      '#ajax'        => array(
        'callback' => array($this, 'validatePhoneAjax'),
        'event'    => 'change',
        'progress' => array(
          'type'    => 'throbber',
          'message' => $this->t('Verifying phone...'),
        ),
      ),
      '#suffix'      => '<span class="phone-ajax-validation"></span>',
      '#required'    => TRUE,
    ];
    $form['location']               = [
      '#type'       => 'textfield',
      '#title'      => $this->t('Location'),
      '#attributes' => array(
        'placeholder' => $this->t('Choose your location'),
      ),
      '#required'   => TRUE,
    ];
    $form['file']                   = array(
      '#type'              => 'managed_file',
      '#title'             => 'File',
      '#progress_message'  => $this->t('File is verifying'),
      '#description'       => $this->t('It could be included only pdf files.'),
      '#upload_validators' => array(
        'file_validate_extensions' => array('pdf'),
        'file_validate_size'       => array(3 * 1024 * 1024),
      ),
      '#required'          => TRUE,
    );
    $form['ticket']                 = [
      '#type'               => 'entity_autocomplete',
      '#title'              => $this->t('Related ticket'),
      '#target_type'        => 'node',
      '#selection_settings' => array(
        'target_bundles' => array('ticket'),
      ),
      '#required'           => TRUE,
    ];
    $form["#attached"]['library'][] = "ajax_form_validation/google_maps";
    $form["#attached"]['library'][] = "ajax_form_validation/ajax_library";
    $form['submit']                 = [
      '#type'  => 'submit',
      '#value' => $this->t('Submit'),
    ];
    return $form;
  }

  protected function validateTitle(array &$form, FormStateInterface $form_state) {
    if (empty($form_state->getValue('title'))) {
      return FALSE;
    }
    return TRUE;
  }

  protected function validateEmail(array &$form, FormStateInterface $form_state) {
    if (empty($form_state->getValue('email')) || !filter_var($form_state->getValue('email'), FILTER_VALIDATE_EMAIL)) {
      return FALSE;
    }
    return TRUE;
  }

  protected function validateName(array &$form, FormStateInterface $form_state) {
    if (empty($form_state->getValue('name')) || strlen($form_state->getValue('name')) <= 3) {
      return FALSE;
    }
    return TRUE;
  }

  protected function validatePhone(array &$form, FormStateInterface $form_state) {
    if (empty($form_state->getValue('phone')) || !preg_match('/^\(\d{3}\)\s?\d{3}-\d{4}$/', $form_state->getValue('phone'))) {
      return FALSE;
    }
    return TRUE;
  }

  protected function validateLocation(array &$form, FormStateInterface $form_state) {
    return empty($form_state->getValue('location'));
  }

  protected function validateTicket(array &$form, FormStateInterface $form_state) {
    return empty($form_state->getValue('ticket'));
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!$this->validateTitle($form, $form_state)) {
      $form_state->setErrorByName('title', $this->t('The title is not be empty.'));
    }
    if (!$this->validateEmail($form, $form_state)) {
      $form_state->setErrorByName('email', $this->t('This is incorrect email address.'));
    }
    if (!$this->validateName($form, $form_state)) {
      $form_state->setErrorByName('name', $this->t('This name is incorrect.'));
    }
    if (!$this->validatePhone($form, $form_state)) {
      $form_state->setErrorByName('phone', $this->t('This phone is incorrect.'));
    }
    if ($this->validateLocation($form, $form_state)) {
      $form_state->setErrorByName('location', $this->t('The location is not be empty.'));
    }
    if ($this->validateTicket($form, $form_state)) {
      $form_state->setErrorByName('ticket', $this->t('The related ticket is not be empty.'));
    }
  }

  public function validateTitleAjax(array &$form, FormStateInterface $form_state) {
    $valid    = $this->validateTitle($form, $form_state);
    $response = new AjaxResponse();
    if ($valid) {
      $css     = ['border' => '2px solid green'];
      $message = $this->t('Title ok.');
    }
    else {
      $css     = ['border' => '2px solid red'];
      $message = $this->t('Title should not be empty.');
    }
    $response->addCommand(new CssCommand('#edit-title', $css));
    $response->addCommand(new HtmlCommand('.title-ajax-validation', $message));
    return $response;
  }

  public function validateEmailAjax(array &$form, FormStateInterface $form_state) {
    $valid    = $this->validateEmail($form, $form_state);
    $response = new AjaxResponse();
    if ($valid) {
      $css     = ['border' => '2px solid green'];
      $message = $this->t('Email ok.');
    }
    else {
      $css     = ['border' => '2px solid red'];
      $message = $this->t('Email not valid.');
    }
    $response->addCommand(new CssCommand('#edit-email', $css));
    $response->addCommand(new HtmlCommand('.email-ajax-validation', $message));
    return $response;
  }

  public function validateNameAjax(array &$form, FormStateInterface $form_state) {
    $valid    = $this->validateName($form, $form_state);
    $response = new AjaxResponse();
    if ($valid) {
      $css     = ['border' => '2px solid green'];
      $message = $this->t('Name is ok');
    }
    else {
      $css     = ['border' => '2px solid red'];
      $message = $this->t('Name not valid');
    }
    $response->addCommand(new CssCommand('#edit-name', $css));
    $response->addCommand(new HtmlCommand('.name-ajax-validation', $message));
    return $response;
  }

  public function validatePhoneAjax(array &$form, FormStateInterface $form_state) {
    $valid    = $this->validatePhone($form, $form_state);
    $response = new AjaxResponse();
    if ($valid) {
      $css     = ['border' => '2px solid green'];
      $message = $this->t('Phone is ok');
    }
    else {
      $css     = ['border' => '2px solid red'];
      $message = $this->t('Phone not valid');
    }
    $response->addCommand(new CssCommand('#edit-phone', $css));
    $response->addCommand(new HtmlCommand('.phone-ajax-validation', $message));
    return $response;
  }

  public function submitForm(
    array &$form, FormStateInterface $form_state
  ) {
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $fid      = $form_state->getValue('file');
    $node     = Node::create(array(
      'type'                 => 'ticket',
      'title'                => $form_state->getValue('title'),
      'langcode'             => $language,
      'uid'                  => \Drupal::currentUser()->id(),
      'field_name'           => array($form_state->getValue('name')),
      'field_email'          => array($form_state->getValue('email')),
      'field_location'       => array($form_state->getValue('location')),
      'field_phone'          => array($form_state->getValue('phone')),
      'field_related_ticket' => array($form_state->getValue('ticket')),
      'field_file'           => array(
        'target_id' => $fid[0],
      ),
    ));
    $node->save();
    return;
  }

}
