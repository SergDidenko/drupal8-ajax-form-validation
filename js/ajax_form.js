(function ($) {
    Drupal.behaviors.results = {
        attach: function (context, settings) {
            $(context).find('#edit-location').on('focus', function () {
                var input = document.getElementById('edit-location');
                var options = {
                    types: ['address']
                };
                var autocomplete = new google.maps.places.Autocomplete(input, options);
            });
        }
    };
})(jQuery);